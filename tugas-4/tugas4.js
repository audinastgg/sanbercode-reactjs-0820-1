// SOAL 1 WHILE
// "LOOPING PERTAMA"
var pertama = 10;
var jumlah1 = 0;

console.log("LOOPING PERTAMA");

while(pertama > 0){
    jumlah1 += 2;

    pertama--;

    
    console.log(+jumlah1 + ' - I Love Coding')
}
console.log('\n')

// "LOOPING KEDUA"
var kedua = 20;

console.log("LOOPING KEDUA");
while(kedua >= 1){
    if (kedua % 2 ==0){

        console.log(kedua+ " - I Will become a Frontend Developer")
    }
    kedua--;
}
console.log('\n')

// SOAL 2
console.log("SOAL KEDUA")
for (var i = 1; i<= 20; i++) {
    if (i % 3 == 0 && i % 2 == 1) {
        console.log(i + " - I Love Coding")
    }else if  (i % 2 == 1) {
        console.log(i + " -  Santai")
    }else if (i % 2 == 0) {
        console.log(i + " - Berkualitas")
    }
    }

    console.log('\n')

// SOAL 3
var i = '';
for(var a=1; a<=7; a++){
    for(var j=1; j<=a; j++){
        i += '#';
    }
    i += '\n';
}
console.log(i);

console.log('\n')

// SOAL 4
var kalimat = "saya sangat senang belajar javascript"

var kata = kalimat.split(" ")
console.log(kata)

console.log('\n')

// SOAL 5
var daftarBuah = [" 2. Apple" , " 5. Jeruk" , " 3. Anggur" , " 4.Semangka" , " 1. Mangga" ];
daftarBuah.sort()
console.log(daftarBuah + '\n' )


    





