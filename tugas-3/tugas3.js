// SOAL 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var upperCase =kataKeempat.toUpperCase();


var gabung = kataPertama + " " + kataKedua+ " " +kataKetiga+ " " +upperCase;

console.log(gabung);
console.log("<br>");


// SOAL 2
var angkaPertama = "1";
var angkaKedua = "2";
var angkaKetiga = "4";
var angkaKeempat = "5";

var strIntPertama = parseInt(angkaPertama);
var strIntKedua = parseInt(angkaKedua);
var strIntKetiga = parseInt(angkaKetiga);
var strIntKeempat = parseInt(angkaKeempat);

var tambah = strIntPertama + strIntKedua + strIntKetiga +strIntKeempat;

console.log(strIntPertama);
console.log(strIntKedua);
console.log(strIntKetiga);
console.log(strIntKeempat);

console.log(tambah);
console.log("<br>");


// SOAL 3
var kalimat = 'wah javascript itu keren sekali';
               //012345678910

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring( 25);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
console.log("<br>");



// SOAL 4
var nilai = 90;

if (nilai >=88) {
    console.log("A");
}
else if (nilai >=70 && nilai < 80){
    console.log("B")
}else if(nilai>=60 && nilai <70 ){
    console.log("C")
}else if(nilai >=50 && nilai <60){
    console.log("D")
}else if(nilai < 50){
    console.log("E")
}

console.log("<br>");



// SOAL 5
var tanggal = 18;
var bulan = "juni";
var tahun = 1995;

switch(tanggal){
   case 0: tanggal = 18;
       break;
}

switch(bulan){
    case 0: {
        console.log('juni');
    }
    break;
}

switch(tahun){
    case 0: tahun = 1995;
    break;
}

var tampilTanggalLahir =  +tanggal+" "+bulan+" "+tahun;

console.log(tampilTanggalLahir);